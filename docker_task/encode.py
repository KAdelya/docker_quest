from cryptography.fernet import Fernet


def encode_func(filename_key, filename):
    with open(filename_key, 'rb') as key_file:
        key = key_file.read()

    fernet = Fernet(key)
    with open(filename, 'rb') as original_file:
        original_data = original_file.read()

    encrypted_data = fernet.encrypt(original_data)

    with open(filename, 'wb') as encrypted_file:
        encrypted_file.write(encrypted_data)


encode_func("crypto.key", "../files/test.txt")
