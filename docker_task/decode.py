from cryptography.fernet import Fernet
import os


def decode_func(filename):
    key = os.getenv('KEY')
    key = key.encode('utf-8')
    fernet = Fernet(key)
    with open(filename, 'rb') as encrypted_file:
        encrypted_data = encrypted_file.read()

    decrypted_data = fernet.decrypt(encrypted_data)
    return decrypted_data
